#!/usr/bin/env bash
#
# Funções utilitárias que ajudam no processo de execução da pipeline.
# Recomenda-se importar esse arquivo via 'source' no 'before_script'.
#
# OBSERVAÇÃO: certifique-se de que o container tenha bash disponível.
#

###############################################################################
# VARIÁVEIS GLOBAIS
###############################################################################

# ANSI escape color codes
ansiVerde='\e[1;32m'
ansiVermelho='\e[1;31m'
ansiSemCor='\e[0m'


# banner():
# Imprime uma mensagem no seguinte formato:
# ============
#  olá mundo!
# ============
banner() {
  local string="$@"
  local length="${#string}"
  local frame

  # artifício para repetir '=' por ${length}+2 vezes
  frame="$(printf '=%.0s' $(seq 1 $((length + 2)) ))"

  echo "${frame}"
  echo " ${string}"
  echo "${frame}"
}


# msgBanner():
# Imprime uma mensagem como em banner(), porém em verde.
msgBanner() {
  echo -en "${ansiVerde}"
  banner "$@"
  echo -en "${ansiSemCor}"
}


# msgBannerErro():
# Imprime uma mensagem como em banner(), porém em vermelho
# e retorna status code 1 (erro).
msgBannerError() {
  echo -en "${ansiVermelho}"
  banner "$@"
  echo -en "${ansiSemCor}"

  return 1
}


# joinBy():
# Imprime os argumentos, separando-os pelo primeiro elemento.
# Exemplo:
# $ joinBy ';' um dois tres
# um;dois;tres
#
# Para mais informações veja https://meleu.sh/join-bash/
joinBy() {
  local IFS="$1"
  echo "${*:2}"
}


unsetProxy() {
  unset \
    http_proxy \
    https_proxy \
    HTTP_PROXY \
    HTTPS_PROXY
}


# setProxy():
# Espera-se que as variáveis 'proxy' e 'noproxy' estejam devidamente
# definidas no arquivo 'proxy_settings.yml'.
setProxy() {
  export http_proxy="${proxy}"
  export https_proxy="${proxy}"
  export HTTP_PROXY="${proxy}"
  export HTTPS_PROXY="${proxy}"
  export no_proxy="$(joinBy ',' ${noproxy})"
  export NO_PROXY="${no_proxy}"
}


showProxySettings() {
  declare -p \
    http_proxy \
    https_proxy \
    HTTP_PROXY \
    HTTPS_PROXY \
    no_proxy \
    NO_PROXY
}
